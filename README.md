# BGMC26 - Prototype
This is a mini game made for the Blender Game Monthly Contest (BGMC) on the Blender Artists forum.

## Concept
You are a Byte missing some Bits of information, parcour the levels to find them back and finally become complete !

## Controls
- `ZQSD` or `WASD` to move.
- `Left-Shift` to sprint.
- `E` or `F` or `Left-Click` to interact.
- `Space` to jump.
