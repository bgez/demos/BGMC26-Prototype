from bgez.framework.types import Asset, GameObject
from bgez.framework.types import Mapped
from bgez.framework import events
from bgez import Service
from bgez import utils

from mathutils import Vector
import aud

@Service.EntityManager.linkToObjects('HoloDesk.000')
class HoloDesk1(Asset, GameObject):
    Reference = './Prototype/Objects/Objects:HoloDesk.000'

    PANEL_LERP = .1

    # Sound controllers
    soundDevice = aud.device()
    popSound = aud.Factory(
        utils.getResourcePath('./Prototype/Sounds/popup_on.ogg'))

    def construct(self):
        self['interactive'] = True
        self.panel = GameObject(self.childrenRecursive.get('HoloPanel.001'))
        self.panel.localScale = Vector((0, 0, 0))

    @Mapped.bind('interact')
    def interact(self):
        self['interactive'] = False
        animation = self.animation(Vector((1, 1, 1)))
        events.execute(animation)
        events.trigger('bit')

        handle = self.soundDevice.play(self.popSound)
        # handle.volume = .3

    async def animation(self, target, frames=30):
        '''This should be a keyframed animation. But oh well...'''
        for i in range(frames):
            lerp = i / frames
            self.panel.localScale = self.panel.localScale.lerp(
                target, lerp)
            await events.skip()
